package com.chamina.student.dal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.chamina.student.dal.entities.Student;

public interface StudentRepository extends CrudRepository<Student, Long> {

}
