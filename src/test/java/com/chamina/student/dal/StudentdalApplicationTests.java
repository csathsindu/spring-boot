package com.chamina.student.dal;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.chamina.student.dal.entities.Student;
import com.chamina.student.dal.repository.StudentRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentdalApplicationTests {
	@Autowired
	private StudentRepository studentRepository;

	@Test
	public void createStudent() {
		Student student = new Student();
		student.setName("chamina");
		student.setCourse("java");
		student.setFee(200.00);
		studentRepository.save(student);
		
	}
	
	@Test
	public void testFindStudentById() {
		Optional<Student> student = studentRepository.findById(1l);
		System.out.println(student);
	}
	
	@Test
	public void updateStudent() {
		Optional<Student> student = studentRepository.findById((long) 1);
		student.get().setFee(4555.00);
		studentRepository.save(student.get() );
	}

}
